---
layout: markdown_page
title: Product categories
---

## Introduction

Below is the canonical list of product categories, grouped by DevOps lifecycle
stage and non-lifecycle groups, along with the name of the responsible product manager.

We want intuitive interfaces both within the company and with the wider community.
This makes it more efficient for everyone to contribute or to get a question answered.
Therefore following interfaces are based on the product categories defined on this page:

- [Product Vision](https://about.gitlab.com/direction/product-vision/)
- [Direction](https://about.gitlab.com/direction/#functional-areas)
- [SDLC](https://about.gitlab.com/sdlc/#stacks)
- [Product Features](https://about.gitlab.com/features/)
- [Documentation](https://docs.gitlab.com/ee/)
- Our deck, the slides that we use to describe the company
- Engineering groups
- Product groups
- Product marketing specializations

![DevOps lifecycle](handbook/sales/devops-loop.svg)

At GitLab the Dev and Ops split is different because our CI/CD functionality is one codebase that falls under Ops.

## Dev

- Product: [Job]
- Backend: [Tommy]

1. Plan - [Victor]
  - Issue management (assignees, milestones, time tracking, due dates, labels, weights, quick actions)
  - Issue boards
  - Email notifications and todos
  - Portfolio management (epics, roadmaps)
  - Search and Elasticsearch integration
  - [Service desk]
  - Chat integration (Mattermost, Slack)
  - Jira and other third-party issue management integration
1. Create - [Victor] and [James]
  - Code review (merge requests, diffs, approvals) - [Victor]
  - Snippets - [Victor]
  - Markdown - [Victor]
  - Git repository management (Commits, file locking, LFS, protected branches, import/export, mirroring, housekeeping (e.g. git gc), hooks) - [James]
  - Web IDE - [James]
  - [Geo] - [James]
  - Wiki - [James]
  - Gitaly - [James]
1. Auth - [Jeremy]
  - Signup
  - User management & authentication (incl. LDAP)
  - GitLab.com (our hosted offering of GitLab)
  - Groups and [Subgroups]
  - Navigation
  - Audit log
  - DevOps Score (previously Conversational Development Index / ConvDev Index)
  - Cycle Analytics
  - [Usage statistics](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) (incl. version.gitlab.com)
  - Subscriptions (incl. license.gitlab.com and customers.gitlab.com)
  - [Internationalization](https://docs.gitlab.com/ee/development/i18n/)
1. Usage statistics - [Victor]
  - Version check
  - Usage ping
1. Gitter - n/a

## Ops

- Product: [Mark]
- Backend: n/a

1. Verify - [Fabio]
  - [Continuous Integration (CI)]
  - GitLab Runner
1. Package - [Fabio]
  - Container Registry
  - Binary Repository
1. Release - [Fabio]
  - [Continuous Delivery (CD)] / Release Automation
  - [Pages]
  - Review apps
1. Configure - [Fabio]
  - Application Control Panel
  - Infrastructure Configuration
  - Operations
  - Feature Management
  - ChatOps
  - PaaS (formerly Auto DevOps)
1. Monitor - [Josh]
  - Metrics
  - Tracing
  - Production monitoring
  - Error Tracking
  - Logging
1. Security Products - [Fabio]
  - Static Application Security Testing (SAST)
  - Dynamic application security testing (DAST)
  - Dependency Scanning
  - Container Scanning
  - License Management
  - Runtime Application Self-Protection (RASP)
1. Distribution - [Josh]
  - Omnibus
  - Cloud Native Installation
1. BizOps - [Josh]

## Composed categories

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)
1. Application Performance Management (APM) = Metrics + Tracing

[Jeremy]: /team/#d3arWatson
[Fabio]: /team/#bikebilly
[Josh]: /team/#joshlambert
[Mark]: /team/#MarkPundsack
[James]: /team/#jamesramsay
[Job]: /team/#Jobvo
[Victor]: /team/#victorwu416
[Tommy]: /team/#tommy.morgan
[Pages]: /features/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /features/service-desk/
